# Branch Pages

Example of how to publish each branch in a subdirectory of a GitLab page. Works for public and private projects.

You can see the master branch here:

https://gnod.gitlab.io/branch-pages/master/

And the development branch here:

https://gnod.gitlab.io/branch-pages/development/

To enable the same in your repo, just copy the .gitlab-ci.yml file.

License: GNU General Public License, version 2
